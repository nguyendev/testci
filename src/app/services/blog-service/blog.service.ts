import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable, throwError } from 'rxjs';
import { BlogEntriesPageable, BlogEntry } from '../../models/blogEntry';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private http: HttpClient) {}

  findOne(id: number): Observable<BlogEntry> {
    return this.http.get<BlogEntry>(
      `${environment.baseUrl}blog-entries/${id}`
    );
  }

  indexAll(page: number, limit: number): Observable<BlogEntriesPageable> {
    let params = new HttpParams();

    params = params.append('page', String(page));
    params = params.append('limit', String(limit));

    return this.http.get<BlogEntriesPageable>(
      `${environment.baseUrl}blog-entries`,
      { params }
    );
  }

  indexByUser(
    userId: number,
    page: number,
    limit: number
  ): Observable<BlogEntriesPageable> {
    let params = new HttpParams();

    params = params.append('page', String(page));
    params = params.append('limit', String(limit));

    return this.http.get<BlogEntriesPageable>(
      `${environment.baseUrl}blog-entries/${String(userId)}`,
      { params }
    );
  }

  post(blogEntry: BlogEntry): Observable<BlogEntry> {
    return this.http.post<BlogEntry>(`${environment.baseUrl}blog-entries`, blogEntry);
  }

  uploadHeaderImage(formData: FormData): Observable<any> {
    return this.http.post<FormData>(
      `${environment.baseUrl}blog-entries/image/upload`,
      formData,
      {
        reportProgress: true,
        observe: 'events',
      }
    );
  }
}
