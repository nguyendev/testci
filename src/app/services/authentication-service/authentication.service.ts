import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { map, switchMap, tap } from 'rxjs/operators';
import { IUser } from '@models/User';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';

export interface LoginForm {
  email: string;
  password: string;
}

export const JWT_TOKEN = 'access_token';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private httpClient: HttpClient,
    private jwtHelper: JwtHelperService
  ) {}

  login(loginForm: LoginForm) {
    return this.httpClient
      .post<any>(`${environment.baseUrl}user/login`, {
        email: loginForm.email,
        password: loginForm.password,
      })
      .pipe(
        map((token) => {
          localStorage.setItem(JWT_TOKEN, token.access_token);
          return token;
        })
      );
  }

  register(user: IUser) {
    return this.httpClient.post<any>(`${environment.baseUrl}user`, user).pipe(
      tap((user) => console.log(user)),
      map((user) => user)
    );
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem(JWT_TOKEN);
    return !this.jwtHelper.isTokenExpired(token);
  }

  logout() {
    localStorage.removeItem(JWT_TOKEN);
  }

  getUserById(): Observable<number> {
    return of(localStorage.getItem(JWT_TOKEN)).pipe(
      switchMap((jwt: string) =>
        of(this.jwtHelper.decodeToken(jwt)).pipe(
          tap((jwt) => {
            console.log(jwt);
          }),
          map((jwt: any) => jwt?.user.id)
        )
      )
    );
  }
}
