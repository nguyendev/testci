import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { fromEvent, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  takeUntil,
} from 'rxjs/operators';
import { IUserData, UserService } from '@services/user-service/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit, OnDestroy, AfterViewInit {
  unsubscribe$ = new Subject();
  filterValue: string = null;
  dataSource: IUserData;
  pageEvent: PageEvent;
  paramName: string;
  @ViewChild('nameSearchInput', { static: true }) nameSearchInput: ElementRef;
  displayedColumn: string[] = ['id', 'name', 'username', 'email', 'role'];

  constructor(
    private userServiceData: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initDataSource();
  }

  initDataSource() {
    this.userServiceData
      .findAll(1, 10)
      .pipe(
        map((userData: IUserData) => (this.dataSource = userData)),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();
  }

  onPaginateChange(event: PageEvent): void {
    let page = event.pageIndex;
    let size = event.pageSize;
    if (this.filterValue === null) {
      page = page + 1;
      this.userServiceData
        .findAll(page, size)
        .pipe(map((userData: IUserData) => (this.dataSource = userData)))
        .subscribe();
    } else {
      this.userServiceData
        .paginateByName(page, size, this.filterValue)
        .pipe(
          map((userData: IUserData) => (this.dataSource = userData)),
          takeUntil(this.unsubscribe$)
        )
        .subscribe();
    }
  }

  findByName(name: string): void {
    this.userServiceData
      .paginateByName(0, 10, name)
      .pipe(
        debounceTime(1000),
        map((userData: IUserData) => (this.dataSource = userData)),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();
  }

  ngAfterViewInit(): void {
    fromEvent(this.nameSearchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        debounceTime(1000),
        distinctUntilChanged(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((name: string) => this.findByName(name));
  }

  navaigateToProfile(id: number) {
    this.router.navigate(['./' + id], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }
}
