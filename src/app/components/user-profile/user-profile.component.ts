import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@environments/environment';
import { IUser } from '@models/User';
import { UserService } from '@services/user-service/user.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  userId: number;
  private sub: Subscription;
  user: IUser;
  imgSrc: string = environment.baseUrl;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.userId = Number(params.id);
      this.userService
        .findOne(this.userId)
        .pipe(map((user) => (this.user = user)))
        .subscribe();
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
