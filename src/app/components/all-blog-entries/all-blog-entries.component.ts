import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { environment } from '@environments/environment';
import { BlogEntriesPageable } from '@models/blogEntry';
import { BlogService } from '@services/blog-service/blog.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-all-blog-entries',
  templateUrl: './all-blog-entries.component.html',
  styleUrls: ['./all-blog-entries.component.css'],
})
export class AllBlogEntriesComponent implements OnInit {
  @Output() paginate: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();
  dataSource: Observable<BlogEntriesPageable> = this.blogService.indexAll(
    1,
    10
  );
  imgSrc = environment.baseUrl;
  pageEvent: PageEvent;

  constructor(private blogService: BlogService) {}

  ngOnInit(): void {
    console.log('AllBlogEntriesComponent')
  }

  onPaginateChange(event: PageEvent) {
    let page = event.pageIndex;
    let size = event.pageSize;

    page = page + 1;

    this.dataSource = this.blogService.indexAll(page, size)
  }
}
